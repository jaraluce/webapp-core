package me.relevante.web.service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import me.relevante.network.Network;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class QueueService {

    private static final Logger logger = LoggerFactory.getLogger(QueueService.class);

    private static final String USER_INDEX_QUEUE = "UserIndex";
    private static final String BULK_ACTIONS_QUEUE = "BulkActions";

    public void sendBulkActionsMessage(String relevanteId,
                                       Class clazz,
                                       ObjectId objectId) {
		JSONObject object = new JSONObject();
		object.put("relevanteId", relevanteId);
        object.put("actionClass", clazz.getSimpleName());
        object.put("objectId", objectId.toString());

        sendMessage(object, BULK_ACTIONS_QUEUE);
    }

    public void sendUserIndexMessage(Network network,
                                     String userId) {
        JSONObject object = new JSONObject();
        object.put("network", network.getName());
        object.put("userId", userId);

        sendMessage(object, USER_INDEX_QUEUE);
    }

	private void sendMessage(JSONObject object, String... queuesNames) {
		String message = object.toString();
		for (String queueName : queuesNames) {
			sendSerializedMessage(queueName, message);
            logger.info("Sent message to queue " + queueName);
		}
	}

    @Autowired
    ConnectionFactory connectionFactory;

    public boolean sendSerializedMessage(String queueName,
                                         String message) {

        if (connectionFactory == null) {
            logger.info("The queues message hasn't been sent to queue: " + queueName);
            return true;
        }

        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(queueName, true, false, false, null);
            channel.basicPublish("", queueName, null, message.getBytes());
        }
        catch (Exception ex){
            logger.error("Error", ex);
            return false;
        }
        finally{
            try {
                if (channel != null) channel.close();
                if (connection != null) connection.close();
            }
            catch (IOException ex) {
                logger.error("Error", ex);
            }
        }
        return true;
    }

}