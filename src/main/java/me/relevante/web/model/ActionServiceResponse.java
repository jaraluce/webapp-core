package me.relevante.web.model;

import me.relevante.model.ActionResult;
import me.relevante.model.ActionStatus;
import me.relevante.model.NetworkAction;

public class ActionServiceResponse {

    private ActionStatus status;
    private ActionResult result;

    public ActionServiceResponse(ActionStatus status,
                                 ActionResult result) {
        this.status = status;
        this.result = result;
    }

    public ActionServiceResponse(NetworkAction networkAction) {
        this.status = networkAction.getStatus();
        this.result = networkAction.getResult();
    }

    public ActionStatus getStatus() {
        return status;
    }

    public ActionResult getResult() {
        return result;
    }
}

