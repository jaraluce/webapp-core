package me.relevante.web.model;

import me.relevante.model.ActionResult;
import me.relevante.model.ActionStatus;
import me.relevante.model.NetworkAction;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ActionProgressOutput {

    private static final String OUTPUT_NONE = "none";
    private static final String OUTPUT_IN_PROGRESS = "inProgress";
    private static final String OUTPUT_DONE = "done";
    private static final String OUTPUT_FORBIDDEN = "forbidden";
    private static final String OUTPUT_ERROR = "error";

    public String getOutput(ActionStatus status,
                            ActionResult result) {
        if (status == null) {
            return OUTPUT_NONE;
        }
        if (status.equals(ActionStatus.IN_PROGRESS)) {
            return OUTPUT_IN_PROGRESS;
        }
        if (result != null) {
            if (result.equals(ActionResult.ALREADY_SUCCEEDED)
                    || result.equals(ActionResult.SUCCESS)
                    || result.equals(ActionResult.USER_MANAGED)) {
                return OUTPUT_DONE;
            }
            if (result.equals(ActionResult.FORBIDDEN)) {
                return OUTPUT_FORBIDDEN;
            }
        }
        return OUTPUT_ERROR;
    }

    public <T extends NetworkAction> String getOutput(List<T> networkActions) {
        if (networkActions == null) {
            return OUTPUT_NONE;
        }
        if (networkActions.isEmpty()) {
            return OUTPUT_NONE;
        }
        T networkAction = networkActions.get(0);
        return getOutput(networkAction.getStatus(), networkAction.getResult());
    }

}
