package me.relevante.web.model.filter;

import me.relevante.model.Blacklist;
import me.relevante.model.NetworkUser;
import me.relevante.model.filter.AbstractFilter;
import me.relevante.model.filter.Filter;

import java.util.ArrayList;
import java.util.List;

public class OutOfBlacklistFilter<T extends NetworkUser> extends AbstractFilter<T> implements Filter<T> {

    private Blacklist blacklist;

    public OutOfBlacklistFilter(Blacklist blacklist) {
        this.blacklist = blacklist;
    }

    @Override
    protected List<T> select(List<T> users) {
        List<T> selectedUsers = new ArrayList<>();
        for (T user : users) {
            if (!this.blacklist.isBlacklistUser(user)) {
                selectedUsers.add(user);
            }
        }
        return selectedUsers;
    }


}
