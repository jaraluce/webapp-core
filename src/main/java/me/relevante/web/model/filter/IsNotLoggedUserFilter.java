package me.relevante.web.model.filter;

import me.relevante.api.NetworkCredentials;
import me.relevante.model.NetworkUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.filter.AbstractFilter;
import me.relevante.model.filter.Filter;

import java.util.ArrayList;
import java.util.List;

public class IsNotLoggedUserFilter<T extends NetworkUser> extends AbstractFilter<T> implements Filter<T> {

    private RelevanteAccount relevanteAccount;

    public IsNotLoggedUserFilter(RelevanteAccount relevanteAccount) {
        this.relevanteAccount = relevanteAccount;
    }

    @Override
    protected List<T> select(List<T> networkUsers) {
        List<T> selectedUsers = new ArrayList<>();
        for (T networkUser : networkUsers) {
            if (!isLoggedUser(networkUser)) {
                selectedUsers.add(networkUser);
            }
        }
        return selectedUsers;
    }

    private boolean isLoggedUser(NetworkUser networkUser) {
        NetworkCredentials networkCredentials = relevanteAccount.getCredentials(networkUser.getNetwork());
        if (networkCredentials == null) {
            return false;
        }
        return (networkCredentials.getUserId().equals(networkUser.getUserId()));
    }

}
