package me.relevante.web.model.filter;

import me.relevante.model.NetworkUser;
import me.relevante.model.filter.AbstractFilter;
import me.relevante.model.filter.Filter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OutOfListFilter<T extends NetworkUser> extends AbstractFilter<T> implements Filter<T> {

    private Set<String> existingKeys;

    public OutOfListFilter(List<T> list) {
        this.existingKeys = new HashSet<>();
        for (T networkUser : list) {
            String key = composeNetworkUserKey(networkUser);
            existingKeys.add(key);
        }
    }

    @Override
    protected List<T> select(List<T> users) {
        List<T> selectedUsers = new ArrayList<>();
        for (T user : users) {
            String key = composeNetworkUserKey(user);
            if (!existingKeys.contains(key)) {
                selectedUsers.add(user);
            }
        }
        return selectedUsers;
    }

    private String composeNetworkUserKey(T networkUser) {
        String key = networkUser.getNetwork().getName() + " " + networkUser.getUserId();
        return key;
    }

}
