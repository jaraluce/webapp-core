package me.relevante.web.model.validation;

import me.relevante.model.NetworkEntity;
import me.relevante.network.Network;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class WrapperValidator<N extends Network, T extends NetworkEntity<N>, U extends NetworkEntity<N>> implements NetworkValidator<N, T> {

    protected NetworkValidator<N, U> wrappedValidator;

    @Autowired
    public WrapperValidator(NetworkValidator<N, U> wrappedValidator) {
        this.wrappedValidator = wrappedValidator;
    }

    @Override
    public boolean isValid(T object) {
        if (object == null) {
            return false;
        }
        U wrappedObject = obtainWrappedObjectFromObject(object);
        boolean isValid = wrappedValidator.isValid(wrappedObject);
        return isValid;
    }

    protected abstract U obtainWrappedObjectFromObject(T user);
}
