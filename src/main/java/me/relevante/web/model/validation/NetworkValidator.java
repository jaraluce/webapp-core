package me.relevante.web.model.validation;

import me.relevante.model.NetworkEntity;
import me.relevante.model.validation.Validator;
import me.relevante.network.Network;

/**
 * Created by daniel-ibanez on 21/07/16.
 */
public interface NetworkValidator<N extends Network, T extends NetworkEntity<N>> extends Validator<T>, NetworkEntity<N> {
}
