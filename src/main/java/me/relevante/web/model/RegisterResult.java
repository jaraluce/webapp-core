package me.relevante.web.model;

import me.relevante.model.NetworkFullUser;
import me.relevante.model.RelevanteAccount;

public class RegisterResult<T extends NetworkFullUser> {

    private RelevanteAccount relevanteAccount;
    private T networkFullUser;
    private boolean isNewUser;


    public RegisterResult(RelevanteAccount relevanteAccount,
                          T networkFullUser,
                          boolean isNewUser) {
        this.relevanteAccount = relevanteAccount;
        this.networkFullUser = networkFullUser;
        this.isNewUser = isNewUser;
    }

    public RelevanteAccount getRelevanteAccount() {
        return relevanteAccount;
    }

    public T getNetworkFullUser() {
        return networkFullUser;
    }

    public boolean isNewUser() {
        return isNewUser;
    }
}
