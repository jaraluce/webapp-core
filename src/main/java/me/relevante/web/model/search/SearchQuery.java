package me.relevante.web.model.search;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 9/05/16.
 */
public class SearchQuery {
    private int maxTerms;
    private List<String> stems;
    private String mainQueryTerm;
    private List<String> leftoverQueryTerms;

    public SearchQuery(List<String> stems,
                       int maxTerms) {
        this.stems = new ArrayList<>(stems);
        this.maxTerms = maxTerms;

        if (stems.size() <= maxTerms) {
            this.mainQueryTerm = String.join("+", stems);
            this.leftoverQueryTerms = new ArrayList<>();
        } else {
            this.mainQueryTerm = String.join("+", stems.subList(0, maxTerms));
            this.leftoverQueryTerms = composeLeftoverQueryStems(stems.subList(maxTerms - 1, stems.size()));
        }
    }

    public String getMainQueryTerm() {
        return mainQueryTerm;
    }

    public List<String> getLeftoverQueryTerms() {
        return leftoverQueryTerms;
    }

    private List<String> composeLeftoverQueryStems(List<String> leftoverStems) {
        List<String> leftoverQueryStems = new ArrayList<>();
        for (int i = 0; i < leftoverStems.size() - 1; i++) {
            for (int j = 1; j < leftoverStems.size(); j++) {
                String queryTerm = leftoverStems.get(i) + "+" + leftoverStems.get(j);
                leftoverQueryStems.add(queryTerm);
            }
        }
        return leftoverQueryStems;
    }

}
