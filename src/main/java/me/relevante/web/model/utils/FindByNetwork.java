package me.relevante.web.model.utils;

import me.relevante.model.NetworkEntity;
import me.relevante.network.Network;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by daniel-ibanez on 18/07/16.
 */
public class FindByNetwork<T extends NetworkEntity> {

    private List<T> list;

    public FindByNetwork(Collection<T> collection) {
        this.list = new ArrayList<>(collection);
    }

    public T find(Network network) {
        for (T element : list) {
            if (element.getNetwork().equals(network)) {
                return element;
            }
        }
        return null;
    }
}
