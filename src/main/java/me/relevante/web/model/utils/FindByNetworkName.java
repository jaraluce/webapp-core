package me.relevante.web.model.utils;

import me.relevante.network.Network;import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by daniel-ibanez on 18/07/16.
 */
public class FindByNetworkName {

    private List<Network> list;

    public FindByNetworkName(Collection<Network> collection) {
        this.list = new ArrayList<>(collection);
    }

    public Network find(String networkName) {
        for (Network network : list) {
            if (network.getName().equals(networkName)) {
                return network;
            }
        }
        throw new IllegalArgumentException();
    }
}
