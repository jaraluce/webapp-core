package me.relevante.web.controller;

public class CoreSessionAttribute {

    public static final String LAST_WORKING_URI = "lastWorkingUri";
    public static final String REDIRECT_URI = "redirectUri";
    public static final String RELEVANTE_ID = "relevanteId";
    public static final String OAUTH_REQUEST_TOKEN =  "oAuthRequestToken";
    public static final String OAUTH_REQUEST_SECRET =  "oAuthRequestSecret";

}
