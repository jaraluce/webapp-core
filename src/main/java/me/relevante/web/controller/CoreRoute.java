package me.relevante.web.controller;

public class CoreRoute {

    public static final String ACCOUNT = "/account";
    public static final String API_PREFIX = "/api";
    public static final String APP_FAIL = "/appFail";
    public static final String CONNECT = "/connect";
    public static final String DISCONNECT = "/disconnect";
    public static final String ERROR = "/error";
    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/logout";
    public static final String WWW_RELEVANTE = "http://www.relevante.me";

}
